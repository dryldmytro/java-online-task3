package com.dmytrodryl.java.exceptions;

public class Main {
    public static void main(String[] args) {
        try (MyAutoClosable closable = new MyAutoClosable();){
            closable.createSome();
        } catch (MyException e) {
            e.printStackTrace();
        }
    }
}
