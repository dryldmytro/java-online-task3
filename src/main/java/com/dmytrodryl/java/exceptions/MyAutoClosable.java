package com.dmytrodryl.java.exceptions;

public class MyAutoClosable implements AutoCloseable {
    public void createSome()throws MyException{
        throw new MyException("Some other message");
    }
    public void close() throws MyException {
        throw new MyException("Some message");
    }

}
