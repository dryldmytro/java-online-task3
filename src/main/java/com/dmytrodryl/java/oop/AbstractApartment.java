package com.dmytrodryl.java.oop;

abstract class AbstractApartment {
    int id;
    int apartmentNumber;
    double apartmentSquare;
    int apartmentFloor;
    int numberOfRooms;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public double getApartmentSquare() {
        return apartmentSquare;
    }

    public void setApartmentSquare(double apartmentSquare) {
        this.apartmentSquare = apartmentSquare;
    }

    public int getApartmentFloor() {
        return apartmentFloor;
    }

    public void setApartmentFloor(int apartmentFloor) {
        this.apartmentFloor = apartmentFloor;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }
}
