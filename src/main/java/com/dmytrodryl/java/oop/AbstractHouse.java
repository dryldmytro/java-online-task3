package com.dmytrodryl.java.oop;

import com.dmytrodryl.java.oop.cheepapart.ArrayCheepApartment;
import com.dmytrodryl.java.oop.elite.ArrayEliteApartment;
import com.dmytrodryl.java.oop.newapartment.ArrayNewApartment;
import java.util.Scanner;

public abstract class AbstractHouse implements ApartmentInterface{
    public String street;
    public String typeOfBuilding;
    public int lifetimeBuilding;

    public AbstractHouse() {
    }

    public AbstractHouse(String street, int lifetimeBuilding) {
        this.street = street;
        this.lifetimeBuilding = lifetimeBuilding;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street)  {
        this.street = street;
    }

    public String getTypeOfBuilding() {
        return typeOfBuilding;
    }

    public void setTypeOfBuilding(String typeOfBuilding) {
        this.typeOfBuilding = typeOfBuilding;
    }

    public int getLifetimeBuilding() {
        return lifetimeBuilding;
    }

    public void setLifetimeBuilding(int lifetimeBuilding) {
        this.lifetimeBuilding = lifetimeBuilding;
    }

    public boolean readyToLife() {
        if (this.lifetimeBuilding>40 || this.lifetimeBuilding==0){
            return false;
        }else return true;
    }
    public void add(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter what type? 0 - Elite, 1 - New, 2 - Cheep");
        int typeOfBuild = scanner.nextInt();
        switch (typeOfBuild){
            case 0:
                ArrayEliteApartment eliteApartment = new ArrayEliteApartment();
                System.out.println("Enter timelife Building");
                int timelifeBuild = scanner.nextInt();
                eliteApartment.add(timelifeBuild);
                break;
            case 1:
                ArrayNewApartment newApartment = new ArrayNewApartment();
                System.out.println("Enter name of street");
                String street = scanner.next();
                newApartment.add(street);
                break;
            case 2:
                ArrayCheepApartment cheepApartment = new ArrayCheepApartment();
                System.out.println("Enter timelife Building");
                int timelifeBuilds = scanner.nextInt();
                System.out.println("Enter name of street");
                String streets = scanner.next();
                cheepApartment.add(streets,timelifeBuilds);
                break;
        }
    }
    public void removeElement(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose which type of Apartment you want delete?");
        System.out.println("0 - Cheep Apartment, 1 - Elite Apartment, 2 - New Apartment");
        int whichTypeApartment = scanner.nextInt();
        switch (whichTypeApartment){
            case 0: ArrayCheepApartment cheepApartment = new ArrayCheepApartment();
            ArrayCheepApartment.showCheepApartment();
                System.out.println("Which apartment you want delete?");
                int whichApartment=  scanner.nextInt();
                cheepApartment.delete(whichApartment);
                ArrayCheepApartment.showCheepApartment();
                break;
            case 1:
                ArrayEliteApartment eliteApartment = new ArrayEliteApartment();
                ArrayEliteApartment.showEliteApart();
                System.out.println("Which apartment you want delete?");
                int whichApartment1=  scanner.nextInt();
                eliteApartment.delete(whichApartment1);
                ArrayEliteApartment.showEliteApart();
                break;
            case 2:
                ArrayNewApartment newApartment = new ArrayNewApartment();
                ArrayNewApartment.showNewApart();
                System.out.println("Which apartment you want delete?");
                int whichApartment2 = scanner.nextInt();
                newApartment.delete(whichApartment2);
                ArrayNewApartment.showNewApart();
                break;
        }
    }
}
