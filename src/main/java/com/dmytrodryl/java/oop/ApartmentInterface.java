package com.dmytrodryl.java.oop;

public interface ApartmentInterface {
    boolean readyToLife();
}
