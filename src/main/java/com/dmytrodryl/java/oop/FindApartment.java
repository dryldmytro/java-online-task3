package com.dmytrodryl.java.oop;

import com.dmytrodryl.java.oop.cheepapart.ArrayCheepApartment;
import com.dmytrodryl.java.oop.elite.ArrayEliteApartment;
import com.dmytrodryl.java.oop.newapartment.ArrayNewApartment;
import java.util.ArrayList;
import java.util.Scanner;

public class FindApartment {
    static Scanner scanner = new Scanner(System.in);
    static ArrayList<ArrayList<String>> arrayYears = new ArrayList<ArrayList<String>>();

    static void init() {
        ArrayCheepApartment.getApartmentInString();
        ArrayEliteApartment.getApartmentInString();
        ArrayNewApartment.getApartmentInString();
        arrayYears.add(ArrayCheepApartment.stringArrayList);
        arrayYears.add(ArrayEliteApartment.stringArrayList);
        arrayYears.add(ArrayNewApartment.stringArrayList);
    }

    static void findOnTypebuilding() {
        System.out.println("Which type you want? 0 - Cheep Apartment, 1 - Elite, 2 - New Apartment");
        Scanner scanner = new Scanner(System.in);
        int whichType = scanner.nextInt();
        switch (whichType) {
            case 0:
                ArrayCheepApartment.showCheepApartment();
                break;
            case 1:
                ArrayEliteApartment.showEliteApart();
                break;
            case 2:
                ArrayNewApartment.showNewApart();
                break;
        }
    }

    static void findOnYears() {
        System.out.println("Enter what year are you interested in?");
        int year = scanner.nextInt();
        for (ArrayList<String> i : arrayYears) {
            for (String x : i) {
                for (String y : x.split("\\D")) {
                    if (Integer.parseInt(y) <= year) {
                        System.out.println(x);
                    }
                }
            }
        }
    }

    static void findOnStreet() {
        System.out.println("Enter what street are you interested in?");
        String street = scanner.next();
        for (ArrayList<String> i : arrayYears){
            for (String x : i){
                String streetName = x.substring(x.lastIndexOf(" ")+1);
                if (streetName.equals(street)){
                    System.out.println(x);
                }
            }
        }
    }
}

