package com.dmytrodryl.java.oop;

import java.util.Scanner;

public enum FindEnum implements FindInterface{
    FIND_ON_TYPEBUILDING("Find on type building"){
        public void resultFind(){
            FindApartment.findOnTypebuilding();
        }
    },
    FIND_ON_YEARS("find on years") {
        public void resultFind() {
            FindApartment.findOnYears();

        }
    },
    FIND_ON_STREET("find on street") {
        public void resultFind() {
FindApartment.findOnStreet();
        }
    };
    private String nameOfMenu;

    FindEnum(String nameOfMenu) {
        this.nameOfMenu = nameOfMenu;
    }

    public String getNameOfMenu() {
        return nameOfMenu;
    }

    static void runFind(){
        for (FindEnum f:FindEnum.values()) {
            System.out.println(f.ordinal()+". "+f.getNameOfMenu());
        }
        Scanner scanner = new Scanner(System.in);
        int userCount = scanner.nextInt();
        FindEnum.resultFind(userCount);

    }

    public static void resultFind(int userChoose) {
        switch (userChoose){
            case 0: FIND_ON_TYPEBUILDING.resultFind();break;
            case 1: FIND_ON_YEARS.resultFind();break;
            case 2: FIND_ON_STREET.resultFind();break;
        }
    }
}
