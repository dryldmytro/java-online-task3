package com.dmytrodryl.java.oop;

import com.dmytrodryl.java.oop.cheepapart.ArrayCheepApartment;
import com.dmytrodryl.java.oop.elite.ArrayEliteApartment;
import com.dmytrodryl.java.oop.newapartment.ArrayNewApartment;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayCheepApartment acp = new ArrayCheepApartment();
        acp.arrayCheepApartment();
        ArrayEliteApartment ea = new ArrayEliteApartment();
        ea.arrayEliteApartment();
        ArrayNewApartment ana = new ArrayNewApartment();
        ana.arrayNewApartment();
        FindApartment.init();
       run();
    }
    public static void run(){
        for (MainMenuEnum menu: MainMenuEnum.values()) {
            System.out.println(menu.ordinal()+". "+menu.getNameOfMenu());
        }
        Scanner scanner = new Scanner(System.in);
        int userCount = scanner.nextInt();
        MainMenuEnum.nextMenu(userCount);

    }
}
