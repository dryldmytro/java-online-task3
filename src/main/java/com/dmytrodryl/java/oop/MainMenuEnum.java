package com.dmytrodryl.java.oop;

import com.dmytrodryl.java.oop.cheepapart.ArrayCheepApartment;
import com.dmytrodryl.java.oop.elite.ArrayEliteApartment;
import com.dmytrodryl.java.oop.newapartment.ArrayNewApartment;

public enum MainMenuEnum implements MainMenuInterface{
    APARTMENT_FIND("Find Apartment") {
        public void nextMenu() {
            FindEnum.runFind();
            Main.run();

        }
    }, APARTMENT_ADD("Add Apartment") {
        public void nextMenu() {
            AbstractHouse as = new AbstractHouse() {
                @Override
                public void add() {
                    super.add();
                }
            };as.add();
            System.out.println("Apartment was add");
            Main.run();
        }
    },
    APARTMENT_DELETE("Delete Apartment") {
        public void nextMenu() {
            AbstractHouse as = new AbstractHouse() {
                @Override
                public void removeElement() {
                    super.removeElement();
                }
            };as.removeElement();
            System.out.println("Element was removed");
            Main.run();

        }
    }, APARTMENT_SHOW("Show All Apartment") {
        public void nextMenu() {
            System.out.println("1. Type of building. 2. Street. 3. Building years.3");
            ArrayEliteApartment.showEliteApart();
            ArrayNewApartment.showNewApart();
            ArrayCheepApartment.showCheepApartment();
            Main.run();
        }
    },
    QUIT{
        public void nextMenu() {
            System.out.println("bye");
System.exit(0);
        }
    };
    private String nameOfMenu;
    MainMenuEnum() {
    }


    MainMenuEnum(String nameOfMenu) {
        this.nameOfMenu = nameOfMenu;
    }

    public String getNameOfMenu() {
        if (nameOfMenu == null) {
            return nameOfMenu = this.name();
        } else
            return nameOfMenu;
    }
    public static void nextMenu(int userChoose){
        switch (userChoose){
            case 0: APARTMENT_FIND.nextMenu();break;
            case 1: APARTMENT_ADD.nextMenu();break;
            case 2: APARTMENT_DELETE.nextMenu();break;
            case 3:APARTMENT_SHOW.nextMenu();break;
            case 4:QUIT.nextMenu();break;
        }
    }
}
