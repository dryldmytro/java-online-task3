package com.dmytrodryl.java.oop.cheepapart;
import java.util.ArrayList;

public class ArrayCheepApartment extends CheepApartment {
    public ArrayCheepApartment() {
        super.setTypeOfBuilding("Cheep");
    }
    public ArrayCheepApartment(String street, int lifetimeBuilding) throws CheepApartmentException {
        super(street, lifetimeBuilding);
        super.setTypeOfBuilding("Cheep Apartment");
    }

    @Override
    public int getLifetimeBuilding() {
        return super.getLifetimeBuilding();
    }

    public static ArrayList<String> stringArrayList = new ArrayList<String>();
    public static ArrayList<CheepApartment> arrayCheepApartment = new ArrayList<CheepApartment>();
    public ArrayList<CheepApartment> arrayCheepApartment(){
        try {
            arrayCheepApartment.add(new CheepApartment("Franko square",20));
            arrayCheepApartment.add(new CheepApartment("Shevchenko str",22));
            arrayCheepApartment.add(new CheepApartment("Railway str",40));
            arrayCheepApartment.add(new CheepApartment("Long str",60));
        } catch (CheepApartmentException e) {
            System.err.println(e.getMessage());
        }
        return arrayCheepApartment;
    }
    public static void showCheepApartment(){
        for (CheepApartment s:arrayCheepApartment) {
            System.out.println(arrayCheepApartment.indexOf(s)+ ". "+s.typeOfBuilding+ " "+ s.street+" "+s.lifetimeBuilding);
        }
    }
    public ArrayList<CheepApartment> add(String street, int lifetimeBuilding){
        try {
            arrayCheepApartment.add(new CheepApartment(street,lifetimeBuilding));
        } catch (CheepApartmentException e) {
            System.err.println(e.getMessage());
        }
        return arrayCheepApartment;
    }
    public ArrayList<CheepApartment> delete(int numberForDelete){
        arrayCheepApartment.remove(numberForDelete);
        return arrayCheepApartment;
    }
    public static ArrayList<String> getApartmentInString(){
        for (CheepApartment s:arrayCheepApartment) {
            stringArrayList.add(s.getLifetimeBuilding()+" "+s.getTypeOfBuilding()+" "+s.getStreet());
        }return stringArrayList;
    }
}
