package com.dmytrodryl.java.oop.cheepapart;

import com.dmytrodryl.java.oop.AbstractHouse;

public class CheepApartment extends AbstractHouse {
    public CheepApartment() {
    }

    public CheepApartment(String street, int lifetimeBuilding) throws CheepApartmentException {
        super(street, lifetimeBuilding);
        super.setTypeOfBuilding("Cheep Apartment");
        if (street.equals("central")){
            throw new CheepApartmentException("Dont have cheep apartment in center");
        }else super.setStreet(street);
        if (lifetimeBuilding<=10){
            throw new CheepApartmentException("Dont have cheep apartment younger 10 stringArrayList");
        }else super.setLifetimeBuilding(lifetimeBuilding);
    }

}

