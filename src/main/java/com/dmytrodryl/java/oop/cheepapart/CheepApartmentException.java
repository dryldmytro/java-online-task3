package com.dmytrodryl.java.oop.cheepapart;

public class CheepApartmentException extends Exception {
    public CheepApartmentException(String message) {
        super(message);
    }
}
