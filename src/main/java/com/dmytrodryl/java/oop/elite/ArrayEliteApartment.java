package com.dmytrodryl.java.oop.elite;

import java.util.ArrayList;

public class ArrayEliteApartment extends EliteApartment {
    public ArrayEliteApartment(String street, int lifetimeBuilding) throws EliteStreetException {
        super(street, lifetimeBuilding);
    }

    public ArrayEliteApartment() {
    }
    public static ArrayList<String> stringArrayList = new ArrayList<String>();
    static ArrayList<EliteApartment> arrayEliteApartment = new ArrayList<EliteApartment>();
    public ArrayList<EliteApartment> arrayEliteApartment(){
        try {
            arrayEliteApartment.add(new EliteApartment("central",5));
            arrayEliteApartment.add(new EliteApartment("central",2));
            arrayEliteApartment.add(new EliteApartment("central",3));
            arrayEliteApartment.add(new EliteApartment("central",1));
        } catch (EliteStreetException e) {
            System.err.println(e.getMessage());
        }
        return arrayEliteApartment;
    }
    public static void showEliteApart(){
        for (EliteApartment s:arrayEliteApartment) {
            System.out.println(arrayEliteApartment.indexOf(s)+". "+s.typeOfBuilding+ " "+ s.street+" "+s.lifetimeBuilding);
        }
    }
    public ArrayList<EliteApartment> add(int lifetimeBuilding){
        try {
            arrayEliteApartment.add(new EliteApartment("central",lifetimeBuilding));
        } catch (EliteStreetException e) {
            System.err.println(e.getMessage());
        }
        return arrayEliteApartment;
    }
    public ArrayList<EliteApartment> delete(int numberForDelete){
        arrayEliteApartment.remove(numberForDelete);
        return arrayEliteApartment;
    }
    public static ArrayList<String> getApartmentInString(){
        for (EliteApartment e:arrayEliteApartment) {
            stringArrayList.add(e.lifetimeBuilding+" "+e.getTypeOfBuilding()+" "+e.getStreet());
        }return stringArrayList;
    }
}
