package com.dmytrodryl.java.oop.elite;

import com.dmytrodryl.java.oop.AbstractHouse;

public class EliteApartment extends AbstractHouse {

    public EliteApartment() {
    }

    public EliteApartment(String street, int lifetimeBuilding) throws EliteStreetException {
        super(street, lifetimeBuilding);
        super.typeOfBuilding = "Elite Apartment";
        if (street.equals("central")) {
            super.setStreet(street);
        } else throw new EliteStreetException("Elite apartment just in Central street");
        if (lifetimeBuilding>10 || lifetimeBuilding<=0){
            throw new EliteStreetException("Elite apartment have 1-10 year");
        }else super.setLifetimeBuilding(lifetimeBuilding);
    }
}
