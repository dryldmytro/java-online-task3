package com.dmytrodryl.java.oop.elite;

public class EliteStreetException extends Exception {
    public EliteStreetException(String message) {
        super(message);
    }
}
