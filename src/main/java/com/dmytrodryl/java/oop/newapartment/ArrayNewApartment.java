package com.dmytrodryl.java.oop.newapartment;

import java.util.ArrayList;

public class ArrayNewApartment extends NewApartment {
    public ArrayNewApartment(String street, int lifetimeBuilding) throws NewApartException {
        super(street, lifetimeBuilding);
    }

    public ArrayNewApartment() {
    }
    public static ArrayList<String>stringArrayList = new ArrayList<String>();
    static ArrayList<NewApartment> arrayNewApartment = new ArrayList<NewApartment>();
    public ArrayList<NewApartment> arrayNewApartment(){
        try {
            arrayNewApartment.add(new NewApartment("central",0));
            arrayNewApartment.add(new NewApartment("Shevchenko st.",0));
            arrayNewApartment.add(new NewApartment("Franko square",0));
            arrayNewApartment.add(new NewApartment("central",0));
        } catch (NewApartException e) {
            System.err.println(e.getMessage());
        }
        return arrayNewApartment;
    }
    public static void showNewApart(){
        for (NewApartment s:arrayNewApartment) {
            System.out.println(arrayNewApartment.indexOf(s)+". "+s.typeOfBuilding+ " "+ s.street+" "+s.lifetimeBuilding);
        }
    }
    public ArrayList<NewApartment> add(String street){
        try {
            arrayNewApartment.add(new NewApartment(street,0));
        } catch (NewApartException e) {
            System.err.println(e.getMessage());
        }
        return arrayNewApartment;
    }
    public ArrayList<NewApartment> delete(int whatDelete){
        arrayNewApartment.remove(whatDelete);
        return arrayNewApartment;
    }
    public static ArrayList<String> getApartmentInString(){
        for (NewApartment na:arrayNewApartment) {
            stringArrayList.add(na.getLifetimeBuilding()+" "+na.getTypeOfBuilding()+" "+na.getStreet());
        }return stringArrayList;
    }
}
