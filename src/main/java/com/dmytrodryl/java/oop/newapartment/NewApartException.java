package com.dmytrodryl.java.oop.newapartment;

public class NewApartException extends Exception {
    public NewApartException(String message) {
        super(message);
    }
}
