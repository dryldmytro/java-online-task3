package com.dmytrodryl.java.oop.newapartment;

import com.dmytrodryl.java.oop.AbstractHouse;

public class NewApartment extends AbstractHouse {
    public NewApartment() {
    }

    public NewApartment(String street, int lifetimeBuilding) throws NewApartException {
        super(street, lifetimeBuilding);
        super.typeOfBuilding="New Apartment";
        super.lifetimeBuilding = 0;
        if (lifetimeBuilding>0){
            throw new NewApartException("New Building just builds");
        } else setLifetimeBuilding(lifetimeBuilding);
    }
}
